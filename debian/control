Source: canid
Section: devel
Priority: optional
Maintainer: Internet Measurement Packaging Team <pkg-netmeasure-discuss@lists.alioth.debian.org>
Uploaders: Iain R. Learmonth <irl@debian.org>
Build-Depends: debhelper (>= 11),
               dh-golang,
               golang-any
Standards-Version: 4.1.4
Homepage: https://github.com/britram/canid
Vcs-Browser: https://salsa.debian.org/ineteng-team/canid
Vcs-Git: https://salsa.debian.org/ineteng-team/canid.git
XS-Go-Import-Path: github.com/britram/canid
Testsuite: autopkgtest-pkg-go

Package: canid
Architecture: any
Built-Using: ${misc:Built-Using}
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Caching Additional Network Information Daemon
 canid the Caching Additional Network Information Daemon provides a simple
 HTTP API for getting information about Internet names and numbers from
 a given vantage point.
 .
 canid looks up BGP AS number and country code associated with the
 smallest prefix announced which contains the address in the RIPEstat
 database. It caches the results by prefix in memory. It returns a JSON
 object with four keys. It also looks up the IPv4 and IPv6 addresses associated
 with a given name. It caches the results by name in memory, and precaches
 prefix results for a subsequent prefix call.
